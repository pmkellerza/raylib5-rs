extern crate bindgen;
extern crate mesonbuild_rs as meson;

use meson::MesonConfig;
use std::process::Command;

fn main() {
    let project_dir = std::env::var("CARGO_MANIFEST_DIR").unwrap();
    println!("cargo:warning=Cargo Manifest Dir: : {}", &project_dir);
    let out_dir = std::env::var("OUT_DIR").unwrap();
    println!("cargo:warning=OUTDIR: {}", &out_dir);
    let build_target = std::env::var("TARGET").unwrap();
    println!("cargo:warning=Build Target: {:?}", &build_target);
    let target_dir = get_cargo_target_dir().ok().unwrap();
    println!("cargo:warning=Target Dir: {:?}", &target_dir);

    println!("cargo:rerun-if-changed=deps/raylib5/src/raygui.c");
    println!("cargo:rerun-if-changed=deps/raylib_wrapper.h");

    //raylib rustc library linking
    println!("cargo:rustc-link-lib=raylib");
    println!("cargo:rustc-link-search={}/install/bin", out_dir);
    println!("cargo:rustc-link-search={}/install/lib", out_dir);
	//println!("cargo:rustc-link-search=C:\\Program Files\\LLVM\\bin");
	//println!("cargo:rustc-link-search=C:\\Program Files\\LLVM\\lib");

    //---------------------- Meson Build Compile Install ----------------------------

    let meson_src_dir = "deps\\raylib5";

    //meson project directory of meson project where meson file and/or native file
    let meson_proj_dir = format!("{}\\{}", project_dir, meson_src_dir);

    //get optimized level
    let profile = std::env::var("PROFILE").unwrap();
    let meson_opt_level = match profile.as_str() {
        "debug" => String::from("debugoptimized"),
        "release" => String::from("release"),
        _ => unreachable!(),
    };

    // Custom Meson Install Dir
    let custom_install_dir = format!("{}\\{}", out_dir, "install");

    //create meson config
    match build_target.as_str() {
        "x86_64-pc-windows-msvc" => {
            let meson = MesonConfig::new(meson_proj_dir, meson_opt_level)
                .have_native_file(true)
                .should_install(true)
                .set_install_dir(custom_install_dir)
                .set_compiler(MesonConfig::MSVC)
                .build();

            meson.run();
        }
        "x86_64-pc-windows-gnu" => {
            let meson = MesonConfig::new(meson_proj_dir, meson_opt_level)
                .have_native_file(true)
                .should_install(true)
                .set_install_dir(custom_install_dir)
                .set_compiler(MesonConfig::GNU)
                .build();

            meson.run();
        }
        _ => unreachable!(),
    }

    //----------------------   Bindgen Wrapper Bindings ----------------------------
    let ray_bindings = bindgen::Builder::default()
        .header("./deps/raylib_wrapper.h")
        .generate()
        .expect("Could not generate Raylib bindings");

    let path = format!("{}\\install", out_dir);
    let out_path = std::path::PathBuf::from(path);

    ray_bindings
        .write_to_file(out_path.join("ray_bindings.rs"))
        .expect("Could not write bindings");

    //---------------------- Copy dll libs to target dir ------------------------------
    let cwd = format!("{}\\install\\bin", out_dir);
    let cmd_status = Command::new("xcopy")
        .current_dir(cwd)
        .args(["*.*", target_dir.to_str().unwrap(), "/Y"])
        .status()
        .expect("");

    assert!(cmd_status.success());
}

fn get_cargo_target_dir() -> Result<std::path::PathBuf, Box<dyn std::error::Error>> {
    let out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR")?);
    let profile = std::env::var("PROFILE")?;
    let mut target_dir = None;
    let mut sub_path = out_dir.as_path();
    while let Some(parent) = sub_path.parent() {
        if parent.ends_with(&profile) {
            target_dir = Some(parent);
            break;
        }
        sub_path = parent;
    }
    let target_dir = target_dir.ok_or("not found")?;
    Ok(target_dir.to_path_buf())
}
