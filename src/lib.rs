#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(improper_ctypes)]

mod constants;
mod core;

mod raylib_ffi;

use raylib_ffi as ffi;

// Prelude.rs
pub mod macros;

//Raylib FFI unsafe export
pub mod ray_ffi {
    pub use crate::ffi::*;
}

pub use ffi::Rectangle;
pub use ffi::Texture2D;

//Core Prelude
pub use constants::*;
pub use core::colors::*;
pub use core::drawing::*;
pub use core::gui::*;
pub use core::window::*;
