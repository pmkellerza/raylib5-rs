use crate::{ffi, rl_str};

pub fn Gui_Button(rect: ffi::Rectangle, text: impl Into<String>) -> bool {
    let text = text.into();

    let res = unsafe { ffi::GuiButton(rect, rl_str!(text)) };

    return match res {
        0 => false,
        1 => true,
        _ => unreachable!(),
    };
}
