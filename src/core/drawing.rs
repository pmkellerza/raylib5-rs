use crate::core::colors::Color;
use crate::{ffi, rl_str};

pub fn Begin_Drawing() {
    unsafe {
        ffi::BeginDrawing();
    }
}

pub fn End_Drawing() {
    unsafe {
        ffi::EndDrawing();
    }
}

pub fn Clear_Background(color: Color) {
    unsafe {
        ffi::ClearBackground(color.into());
    }
}

pub fn Draw_Text(text: impl Into<String>, posx: i32, posy: i32, font_size: i32, color: Color) {
    let text = text.into();
    unsafe { ffi::DrawText(rl_str!(text), posx, posy, font_size, color.into()) }
}

pub fn Load_Texture(texture_path: impl Into<String>) -> ffi::Texture2D {
    let texture_path = texture_path.into();

    unsafe { ffi::LoadTexture(rl_str!(texture_path)) }
}

pub fn Unload_Texture(texture: ffi::Texture2D) {
    unsafe {
        ffi::UnloadTexture(texture);
    }
}
