use crate::{ffi, rl_str};

pub fn Init_Window(width: i32, height: i32, title: impl Into<String>) {
    let title = title.into();
    unsafe {
        ffi::InitWindow(width, height, rl_str!(title));
    }
}

pub fn Close_Window() {
    unsafe {
        ffi::CloseWindow();
    }
}

pub fn Window_Should_Close() -> bool {
    unsafe { ffi::WindowShouldClose() }
}
